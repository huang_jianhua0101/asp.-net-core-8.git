﻿using Microsoft.AspNetCore.Mvc;
using IActionResultExample.Models;

namespace IActionResultExample.Controllers
{
    public class HomeController : Controller
    {
        [Route("bookstore")]
        public IActionResult Index()
        {
            //Book id should be applied
            if (!Request.Query.ContainsKey("bookid"))
            {
                //return Content("Book id is not supplied");
                //return new BadRequestResult();
                return BadRequest("Book id is not supplied");
            }

            //Book id can't be empty
            if (string.IsNullOrEmpty(Convert.ToString(Request.Query["bookid"])))
            {
                //Response.StatusCode = 400;
                //return Content("Book id can't be null or empty");
                return BadRequest("Book id can't be null or empty");
            }

            //Book id should be between 1 to 1000
            int bookId = Convert.ToInt32(ControllerContext.HttpContext.Request.Query["bookid"]);
            if (bookId <= 0)
            {
                //Response.StatusCode = 400;
                //return Content("Book id can't be less than or equal to zero");
                return BadRequest("Book id can't be less than or equal to zero");
            }
            if (bookId > 1000)
            {
                //Response.StatusCode = 400;
                //return Content("Book id can't be greater than 1000");
                return NotFound("Book id can't be greater than 1000");
            }

            if (!Convert.ToBoolean(Request.Query["isloggedin"]))
            {
                //Response.StatusCode = 401;
                //return Content("User must be authenticated");
                //return Unauthorized("User must be authenticated");
                return StatusCode(401, "User must be authenticated");
            }

            //return File("/Sample.pdf", "application/pdf");
            //return new RedirectToActionResult("Books", "Store", "store/books");
            //return RedirectToAction("Books", "Store", new {id = bookId});
            //return new RedirectToActionResult("Books", "Store", "store/books",true);
            //return new RedirectToActionResult("Books", "Store", "store/books", permanent: true);
            //return RedirectToActionPermanent("Books", "Store", new { id = bookId });

            //return new LocalRedirectResult($"~/store/books/{bookId}");
            //return LocalRedirect($"~/store/books/{bookId}");

            //return Redirect("https://www.baidu.com");
            return Redirect($"~/store/books/{bookId}");
        }

        [Route("modelbd/{bookid?}/{isloggedin?}")]
        public IActionResult Index2(int? bookid, bool? isloggedin, 
            Book book)
        {
            //Book id should be applied
            if (bookid.HasValue == false)
            {
                return BadRequest("Book id is not supplied");
            }

            //Book id can't be empty
            if (string.IsNullOrEmpty(bookid.ToString()))
            {
                return BadRequest("Book id can't be null or empty");
            }

            //Book id should be between 1 to 1000
            if (bookid <= 0)
            {
                return BadRequest("Book id can't be less than or equal to zero");
            }
            if (bookid > 1000)
            {
                return NotFound("Book id can't be greater than 1000");
            }

            if (isloggedin == false)
            {
                return StatusCode(401, "User must be authenticated");
            }

            return Content($"Book Id: {bookid}, isloggedin: {isloggedin}, Book: {book.ToString()}", "text/plain");
        }
    }
}
