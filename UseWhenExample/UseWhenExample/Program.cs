var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

//app.MapGet("/", () => "Hello World!");

app.UseWhen(
    context => context.Request.Query.ContainsKey("username"),
    app =>
    {
        app.Use(async (context,next) =>
        {
            await context.Response.WriteAsync($"<p>Hello from Middleware branch</p>");
            await next(context);
        });
    });

app.Use(async (context, next) =>
{
    await context.Response.WriteAsync($"<p>Hello from Middleware at main chain</p>");
    await next(context);
});

app.Run();
