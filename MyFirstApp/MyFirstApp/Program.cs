
//加载配置、环境变量和服务；
//配置：连接字符串等自定义配置；
//环境变量：API URLs/服务器名称等；
//服务：预定义和自定义服务；
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Primitives;

var builder = WebApplication.CreateBuilder(args);
//builder.Configuration;
//builder.Environment;
//builder.Services;

var app = builder.Build();
//app可以用来添加中间件


//app.MapGet("/", () => "Hello World!");

app.Run(async (HttpContext context) =>
{
    //context.Response.Headers["MyKey"] = "My Value";
    context.Response.Headers.Append("MyKey", "My Value");
    //context.Response.Headers["Content-Type"] = "text/html";
    context.Response.Headers.ContentType = "text/html";

    context.Response.StatusCode = 200;
    await context.Response.WriteAsync("<h1>Hello</h1>");
    await context.Response.WriteAsync("<h2> World</h2>");

    string path = context.Request.Path;
    await context.Response.WriteAsync($"<p>{path}</p>");

    string method = context.Request.Method; 
    await context.Response.WriteAsync($"<p>{method}</p>");

    if (context.Request.Method == "GET")
    {
        if(context.Request.Query.ContainsKey("id"))
        {
            string id = context.Request.Query["id"];
            await context.Response.WriteAsync($"<p>{id}</p>");
        }
    }

    if (context.Request.Headers.ContainsKey("User-Agent"))
    {
        string userAgent = context.Request.Headers["User-Agent"];
        await context.Response.WriteAsync($"<p>{userAgent}</p>");
    }
    
    if (context.Request.Headers.ContainsKey("AuthorizationKey"))
    {
        string AuthorizationKey = context.Request.Headers["AuthorizationKey"];
        await context.Response.WriteAsync($"<p>{AuthorizationKey}</p>");
    }

    if (context.Request.Method == "POST")
    {
        using (StreamReader reader = new StreamReader(context.Request.Body))
        {
            //读取原始数据
            string postData = await reader.ReadToEndAsync();
            await context.Response.WriteAsync($"<p>{postData}</p>");

            //将请求体转化成键值对，使用StringValues是因为可能有多个值，比如firstName=kevin&age=20&age=30
            Dictionary<string, StringValues> queryDict = QueryHelpers.ParseQuery(postData);

            if (queryDict.ContainsKey("firstName"))
            {
                string firstName = queryDict["firstName"][0];
                await context.Response.WriteAsync($"<p>{firstName}</p>");
                ////以下方式也可
                //foreach (var item in queryDict["firstName"])
                //{

                //}
            }
        }
    }

});

app.Run();
