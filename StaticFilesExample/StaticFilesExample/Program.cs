//var builder = WebApplication.CreateBuilder(args);
using Microsoft.Extensions.FileProviders;

var builder = WebApplication.CreateBuilder(new
    WebApplicationOptions()
{
    WebRootPath = "myroot"
});

var app = builder.Build();

app.UseStaticFiles();   //works with the web root path(myroot)

app.UseStaticFiles(new StaticFileOptions()  //works with mywebroot
{
    FileProvider = new PhysicalFileProvider(
        Path.Combine(builder.Environment.ContentRootPath, "mywebroot")
        )
});
app.UseRouting();

app.UseEndpoints(endpoints => 
{
    endpoints.Map("/", async context =>
    {
        await context.Response.WriteAsync("Hello");
    });
    
});
//app.MapGet("/", () => "Hello World!");

app.Run();
