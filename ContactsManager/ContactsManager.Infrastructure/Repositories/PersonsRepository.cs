﻿using Entities;
using RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Repositories
{
    public class PersonsRepository : IPersonsRepository
    {
        private readonly ApplicationDbContext _db;
        private readonly ILogger<PersonsRepository> _logger;

        public PersonsRepository(ApplicationDbContext db, ILogger<PersonsRepository> logger = null)
        {
            _db = db;
            _logger = logger;
        }

        public async Task<Person?> AddPersonAsync(Person person)
        {
            await _db.Persons.AddAsync(person);
            await _db.SaveChangesAsync();
            return person;
        }

        public async Task<bool> DeletePersonByPersonIdAsync(Guid PersonId)
        {
            _db.Persons.RemoveRange(_db.Persons.Where(p => p.PersonId == PersonId));
            int rowsDeleted = await _db.SaveChangesAsync();
            return rowsDeleted > 0;
        }

        public async Task<IEnumerable<Person>?> GetAllPersonsAsync()
        {
            _logger.LogInformation("【PersonsService】- GetAllPersonsAsync of PersonsService");
            return await _db.Persons.Include("Country").ToListAsync();
        }

        public async Task<IEnumerable<Person>?> GetFilteredPersonsAsync(Expression<Func<Person, bool>> predicate)
        {
            _logger.LogInformation("【PersonsService】- GetFilteredPersonsAsync of PersonsService");
            return await _db.Persons.Include("Country")
                .Where(predicate)
                .ToListAsync();
        }

        public async Task<Person?> GetPersonByPersonIdAsync(Guid personId)
        {
            return await _db.Persons.Include("Country")
                .FirstOrDefaultAsync(p => p.PersonId == personId);
        }

        public async Task<Person?> UpdatePersonAsync(Person person)
        {
            Person? matchingPerson = await _db.Persons.FirstOrDefaultAsync(p => p.PersonId == person.PersonId);
            if (matchingPerson == null)
            {
                return person;
            }
            matchingPerson.PersonName = person.PersonName;
            matchingPerson.Email = person.Email;
            matchingPerson.DateOfBirth = person.DateOfBirth;
            matchingPerson.Gender = person.Gender;
            matchingPerson.CountryId = person.CountryId;
            matchingPerson.Address = person.Address;
            matchingPerson.ReceiveNewsLetters = person.ReceiveNewsLetters;
            int countUpdated = await _db.SaveChangesAsync();
            return matchingPerson;  
        }
    }
}
