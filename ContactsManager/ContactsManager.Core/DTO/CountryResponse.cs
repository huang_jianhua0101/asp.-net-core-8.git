﻿using System;
using System.Runtime.CompilerServices;
using Entities;

namespace ServiceContracts.DTO
{
    /// <summary>
    /// DTO class that is used as return type for most of CountriesService methods
    /// </summary>
    public class CountryResponse
    {
        public Guid CountryId { get; set; } 
        public string? CountryName { get; set; }


        //It compares the current object to another object of CountryResponse type and returns true,
        //if both values are same; otherwise returns false
        public override bool Equals(object? obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (obj.GetType() != typeof(CountryResponse))
            {
                return false;
            }

            CountryResponse countryToCompare = (CountryResponse)obj;

            return (this.CountryId == countryToCompare.CountryId && 
                this.CountryName == countryToCompare.CountryName);
        }

        //根据提示添加，为了没有绿色波浪线报错
        public override int GetHashCode()
        {
            return base.GetHashCode();  
        }
    }

    public static class CounteryEntensions
    {
        public static CountryResponse ToCountryResponse(this Country country)
        {
            return new CountryResponse()
            {
                CountryId = country.CountryId,
                CountryName = country.CountryName,
            };
        }
    }
}
