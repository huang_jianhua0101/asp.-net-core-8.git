﻿using System;
using Entities;
using ServiceContracts.DTO;
using ServiceContracts;
using Services.Helpers;
using ServiceContracts.Enums;
using CsvHelper;
using System.Globalization;
using CsvHelper.Configuration;
using System.Reflection;
using OfficeOpenXml;
using System.ComponentModel.DataAnnotations.Schema;
using RepositoryContracts;
using Microsoft.Extensions.Logging;
using Serilog;
using SerilogTimings;
using Exceptions;

namespace Services
{
    public class PersonsAdderService : IPersonsAdderService
    {
        //private field
        private readonly IPersonsRepository _personsRepository;
        private readonly ILogger<PersonsAdderService> _logger;

        private readonly IDiagnosticContext _diagnosticContext;

        //constructor
        public PersonsAdderService(IPersonsRepository personsRepository, ILogger<PersonsAdderService> logger = null, IDiagnosticContext diagnosticContext = null)
        {
            _personsRepository = personsRepository;
            _logger = logger;
            _diagnosticContext = diagnosticContext;
        }

        public async Task<PersonResponse> AddPerson(PersonAddRequest? personAddRequest)
        {
            //check if PersonAddRequest is not null
            if (personAddRequest == null)
            {
                throw new ArgumentNullException(nameof(personAddRequest));
            }

            //Model validation
            ValidationHelper.ModelValidation(personAddRequest);

            //convert personAddRequest into Person type
            Person person = personAddRequest.ToPerson();

            //generate PersonId
            person.PersonId = Guid.NewGuid();

            //add person object to persons list
            await _personsRepository.AddPersonAsync(person);
            //_db.sp_InsertPerson(person);


            //convert the Person object into PersonResponse type
            return person.ToPersonResponse();
        }
    }
}