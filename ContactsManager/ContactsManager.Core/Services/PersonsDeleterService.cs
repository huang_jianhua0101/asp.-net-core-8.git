﻿using System;
using Entities;
using ServiceContracts.DTO;
using ServiceContracts;
using Services.Helpers;
using ServiceContracts.Enums;
using CsvHelper;
using System.Globalization;
using CsvHelper.Configuration;
using System.Reflection;
using OfficeOpenXml;
using System.ComponentModel.DataAnnotations.Schema;
using RepositoryContracts;
using Microsoft.Extensions.Logging;
using Serilog;
using SerilogTimings;
using Exceptions;

namespace Services
{
    public class PersonsDeleterService : IPersonsDeleterService
    {
        //private field
        private readonly IPersonsRepository _personsRepository;
        private readonly ILogger<PersonsDeleterService> _logger;

        private readonly IDiagnosticContext _diagnosticContext;

        //constructor
        public PersonsDeleterService(IPersonsRepository personsRepository, ILogger<PersonsDeleterService> logger = null, IDiagnosticContext diagnosticContext = null)
        {
            _personsRepository = personsRepository;
            _logger = logger;
            _diagnosticContext = diagnosticContext;
        }

        public async Task<bool> DeletePerson(Guid? PersonId)
        {
            if (PersonId == null)
            {
                throw new ArgumentNullException(nameof(PersonId));
            }

            Person? person = await _personsRepository.GetPersonByPersonIdAsync(PersonId.Value);
            if (person == null)
                return false;

            await _personsRepository.DeletePersonByPersonIdAsync(PersonId.Value);

            return true;
        }
    }
}