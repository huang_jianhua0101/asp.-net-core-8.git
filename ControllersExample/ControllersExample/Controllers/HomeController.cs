﻿using Microsoft.AspNetCore.Mvc;
using ControllersExample.Models;

namespace ControllersExample.Controllers
{
    [Controller]
    public class HomeController : Controller
    {
        #region
        //[Route("home")]
        //[Route("/")]
        //public string Index()
        //{
        //    return "Hello from Index";
        //}
        //[Route("about")]
        //public string About()
        //{
        //    return "Hello from About";
        //}
        //[Route("contact-us/{mobile:regex(^\\d{{10}}$)}")]
        //public string Contact()
        //{
        //    return "Hello from Contact";
        //}
        #endregion

        [Route("home")]
        [Route("/")]
        public ContentResult Index()
        {
            //return new ContentResult()
            //{
            //    Content = "<p>Hello from Index</p>",
            //    ContentType = "text/html"
            //};

            //如果需要使用Content方法，添加的Controller类需要继承Microsoft.AspNetCore.Mvc.Controller
            return Content("<p>Hello from Index</p>","text/html");
        }

        [Route("person")]
        public JsonResult Person()
        {
            Person person = new Person()
            {
                Id = Guid.NewGuid(),
                FirstName = "Jianhua",
                LastName = "Huang",
                Age = 26
            };
            //return new JsonResult(person);
            return Json(person);  //实际情况优先使用
        }

        //文件在wwwroot文件夹内
        [Route("file-download")]
        public VirtualFileResult FileDownload()
        {
            //return new VirtualFileResult("/Sample.pdf", "application/pdf");
            return File("/Sample.pdf", "application/pdf");
        }
        //文件在wwwroot文件夹外
        [Route("file-download2")]
        public PhysicalFileResult FileDownload2()
        {
            //return new PhysicalFileResult(@"C:\Users\huang_jianhua0101\source\repos\asp.-net-core-8\ControllersExample\ControllersExample\Sample2.pdf", "application/pdf");
            return PhysicalFile(@"C:\Users\huang_jianhua0101\source\repos\asp.-net-core-8\ControllersExample\ControllersExample\Sample2.pdf", "application/pdf");
        }
        //从别的地方读取文件，从数据库读取文件，文件加密，提供部分文件等情况下使用
        [Route("file-download3")]
        public FileContentResult FileDownload3()
        {
            string filePath = @"C:\Users\huang_jianhua0101\source\repos\asp.-net-core-8\ControllersExample\ControllersExample\Sample2.pdf";
            byte[] bytes = System.IO.File.ReadAllBytes(filePath);
            //return new FileContentResult(bytes, "application/pdf");
            return File(bytes, "application/pdf");
        }
    }
}
