using ControllersExample.Controllers;

var builder = WebApplication.CreateBuilder(args);

//builder.Services.AddTransient<HomeController, HomeController>();    //启用单个Controller
builder.Services.AddControllers();  //启用所有Controller，实际运用中用此方法

var app = builder.Build();

app.UseStaticFiles();
//app.UseRouting();
//app.UseEndpoints(endpoints =>
//{
//    endpoints.MapControllers();
//});
app.MapControllers(); //相当于上面注释部分

//app.MapGet("/", () => "Hello World!");

app.Run();
