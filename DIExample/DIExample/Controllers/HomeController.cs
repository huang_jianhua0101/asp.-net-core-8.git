﻿using Autofac;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using ServiceContracts;
using Services;

namespace DIExample.Controllers
{
    public class HomeController : Controller
    {
        //private readonly CitiesService _citiesService;

        private readonly ICitiesService _citiesService1;
        private readonly ICitiesService _citiesService2;
        private readonly ICitiesService _citiesService3;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        private readonly ILifetimeScope _lifeTimeScope; //Autofac

        public HomeController(ICitiesService services1,
            ICitiesService services2,
            ICitiesService services3,
            IServiceScopeFactory serviceScopeFactory,
            ILifetimeScope lifeTimeScope)
        {
            //_citiesService = new CitiesService();
            _citiesService1 = services1;
            _citiesService2 = services2;
            _citiesService3 = services3;
            _serviceScopeFactory = serviceScopeFactory;
            _lifeTimeScope = lifeTimeScope;
        }

        [Route("/")]
        public IActionResult Index()
        {
            List<string> cities = _citiesService1.GetCities();
            ViewBag.Ins1 = _citiesService1.ServiceInstanceId;
            ViewBag.Ins2 = _citiesService2.ServiceInstanceId;
            ViewBag.Ins3 = _citiesService3.ServiceInstanceId;

            //using(IServiceScope scope = _serviceScopeFactory.CreateScope())
            //{
            //    //Inject CitiesService
            //    ICitiesService citiesService = scope.ServiceProvider.GetRequiredService<ICitiesService>();
            //    //DB work

            //    ViewBag.Ins4 = citiesService.ServiceInstanceId;
            //} //end of scope; it calls CitiesService.Dispose()

            using (ILifetimeScope scope = _lifeTimeScope.BeginLifetimeScope())
            {
                //Inject CitiesService
                ICitiesService citiesService = scope.Resolve<ICitiesService>();
                //DB work

                ViewBag.Ins4 = citiesService.ServiceInstanceId;
            } //end of scope; it calls CitiesService.Dispose()
            return View(cities);
        }
    }
}
