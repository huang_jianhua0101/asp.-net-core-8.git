﻿using HttpClientStocksApp.Models;
using HttpClientStocksApp.ServiceContracts;
using HttpClientStocksApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace HttpClientStocksApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IWeatherService _weatherService;

        public HomeController(IWeatherService weatherService)
        {
            _weatherService = weatherService;
        }

        [Route("/")]
        public async Task<IActionResult> Index()
        {
            Dictionary<string, object>? responseDict = await _weatherService.GetWeatherInfo();
            OneDayWeather oneDayWeather = new OneDayWeather()
            {
                Message = responseDict["message"].ToString(),
                Status = responseDict["status"].ToString(),
                Date = Convert.ToDateTime(responseDict["time"].ToString())
            };

            return View(oneDayWeather);
        }
    }
}
