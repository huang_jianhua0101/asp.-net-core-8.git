﻿namespace HttpClientStocksApp.ServiceContracts
{
    public interface IWeatherService
    {
        Task<Dictionary<string, object>?> GetWeatherInfo();
    }
}
