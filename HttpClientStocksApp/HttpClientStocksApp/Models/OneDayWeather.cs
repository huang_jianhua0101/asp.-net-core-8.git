﻿namespace HttpClientStocksApp.Models
{
    public class OneDayWeather
    {
        public string? Message { get; set; }
        public string? Status { get; set;}
        public DateTime? Date { get; set; }
    }
}
