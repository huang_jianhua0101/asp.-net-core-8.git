﻿using HttpClientStocksApp.ServiceContracts;
using System.Data;
using System.Text.Json;

namespace HttpClientStocksApp.Services
{
    public class WeatherService : IWeatherService
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public WeatherService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<Dictionary<string, object>?> GetWeatherInfo()
        {
            using (HttpClient httpClient = _httpClientFactory.CreateClient())
            {
                //需要发送出去的消息
                HttpRequestMessage httpRequestMessage = new HttpRequestMessage()
                {
                    RequestUri = new Uri("http://t.weather.sojson.com/api/weather/city/101030100"),
                    Method = HttpMethod.Get,
                };
                //发送消息并获取响应内容
                HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);
                //读取响应的内容并存到Dictionary中
                Stream stream = await httpResponseMessage.Content.ReadAsStreamAsync();
                StreamReader reader = new StreamReader(stream); 
                string response = reader.ReadToEnd();   
                Dictionary<string, object>? responseDict = JsonSerializer.Deserialize<Dictionary<string, object>>(response);

                //验证responseDict
                if (responseDict == null)
                {
                    throw new InvalidOperationException("No response");
                }
                if (responseDict.ContainsKey("error"))
                {
                    throw new InvalidOperationException(Convert.ToString(responseDict["error"]));
                }
                return responseDict;
            }
        }
    }
}
