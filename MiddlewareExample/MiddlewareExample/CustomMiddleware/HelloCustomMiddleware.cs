﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace MiddlewareExample.CustomMiddleware
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class HelloCustomMiddleware
    {
        private readonly RequestDelegate _next;

        public HelloCustomMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            //httpContext.Response.ContentType = "text/html"; 发现写了以后后续管道不通
            //确保在设置ContentType后不要再调用httpContext.Response.WriteAsync
            //或httpContext.Response.End()，这样会结束响应，导致后续中间件不再执行。
            //如果你确实需要在中间件中返回HTML并且确保后续中间件可以执行，
            //你可以在设置ContentType后通过httpContext.Response.Body写入HTML内容，
            //而不是直接使用Response.WriteAsync。
            if (httpContext.Request.Query.ContainsKey("firstname") &&
                httpContext.Request.Query.ContainsKey("lastname"))
            {
                string fullname = httpContext.Request.Query["firstname"] + " "
                    + httpContext.Request.Query["lastname"];
                await httpContext.Response.WriteAsync($"<p>{fullname}</p>");
            }
            //else
            //{
            //    await httpContext.Response.WriteAsync($"<p>There is no firstname and lastname!</p>");
            //}
            await _next(httpContext);
            await httpContext.Response.WriteAsync($"<p>After</p>");
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class HelloCustomMiddlewareExtensions
    {
        public static IApplicationBuilder UseHelloCustomMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<HelloCustomMiddleware>();
        }
    }
}
