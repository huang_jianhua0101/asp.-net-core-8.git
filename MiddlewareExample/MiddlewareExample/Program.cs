using MiddlewareExample.CustomMiddleware;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddTransient<MyCustomMiddleware>();

var app = builder.Build();

//app.MapGet("/", () => "Hello World!");

//018
//app.Run(async (HttpContext context) =>
//{
//    await context.Response.WriteAsync("Hello Middleware");
//});

////����ִ��
//app.Run(async (HttpContext context) =>
//{
//    await context.Response.WriteAsync("Hello Middleware2");
//});

//019
app.Use(async (HttpContext context, RequestDelegate next) =>
{
    context.Response.ContentType = "text/html";
    await context.Response.WriteAsync("<p>Hello Middleware1-1</p>");
    await next(context);
    await context.Response.WriteAsync("<p>Hello Middleware1-2</p>");
});

app.Use(async (HttpContext context, RequestDelegate next) =>
{
    await context.Response.WriteAsync("<p>Hello Middleware2-1</p>");
    await next(context);
    await context.Response.WriteAsync("<p>Hello Middleware2-2</p>");
});

//app.UseMiddleware<MyCustomMiddleware>();

//021
//app.UseMyCustomMiddleware();

//022
app.UseHelloCustomMiddleware();

app.Run(async (HttpContext context) =>
{
    await context.Response.WriteAsync("<p>Hello Middleware3</p>");
});

app.Run();
