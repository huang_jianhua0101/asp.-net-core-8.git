using ConfigurationExample.ConfigClasses;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllersWithViews();

//Supply an object of WeatherAPIOptions (with 'weatherapi' section) as a service
builder.Services.Configure<WeatherAPIOptions>(
    builder.Configuration.GetSection("weatherapi"));

//Load MyOwnConfig.json
builder.Host.ConfigureAppConfiguration((hostingContext, config) =>
{
    config.AddJsonFile("MyOwnConfig.json", optional: true,
        reloadOnChange: true);
});

var app = builder.Build();

//app.MapGet("/", () => "Hello World!");

app.UseStaticFiles();
app.UseRouting();

//app.UseEndpoints(endpoints =>
//{
//    endpoints.Map("/config", async context =>
//    {
//        await context.Response.WriteAsync(app.Configuration["MyKey"] + "\n");
//        await context.Response.WriteAsync(app.Configuration.GetValue<string>("MyKey") + "\n");
//        await context.Response.WriteAsync(app.Configuration.GetValue<string>("MyKey1","Default") + "\n");
//    });
//});
app.MapControllers();

app.Run();
