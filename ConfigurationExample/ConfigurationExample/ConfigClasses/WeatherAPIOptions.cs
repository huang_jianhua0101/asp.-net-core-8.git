﻿namespace ConfigurationExample.ConfigClasses
{
    public class WeatherAPIOptions
    {
        public string? ClientId {  get; set; }   
        public string? ClientSecret { get; set; }
    }
}
