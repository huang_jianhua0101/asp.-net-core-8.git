﻿using ConfigurationExample.ConfigClasses;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace ConfigurationExample.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration _configuration;

        private readonly WeatherAPIOptions _weatherAPIOptions;

        public HomeController(IConfiguration configuration, IOptions<WeatherAPIOptions> weatherAPIOptions)
        {
            _configuration = configuration;
            _weatherAPIOptions = weatherAPIOptions.Value;
        }

        [Route("/")]
        public IActionResult Index()
        {
            ViewBag.MyKey = _configuration["MyKey"];
            ViewBag.MyAPIKey = _configuration.GetValue<string>("MyAPIKey",
                "The Default Key");

            //ViewBag.ClientID = _configuration["weatherapi:ClientID"];
            //ViewBag.ClientSecret = _configuration.GetValue<string>("weatherapi:ClientSecret", "");

            //ViewBag.ClientID = _configuration.GetSection("weatherapi")["ClientID"];
            //ViewBag.ClientSecret = _configuration.GetSection("weatherapi")["ClientSecret"];

            //IConfiguration weatherapiSection = _configuration.GetSection("weatherapi");
            //ViewBag.ClientID = weatherapiSection["ClientID"];
            //ViewBag.ClientSecret = weatherapiSection["ClientSecret"];

            ////Recommended, Loads configuration values into a new Options object
            //WeatherAPIOptions? wapi = _configuration.GetSection("weatherapi").Get<WeatherAPIOptions>();
            //ViewBag.ClientID = wapi.ClientId;
            //ViewBag.ClientSecret = wapi.ClientSecret;

            ////Loads configuration values into exsiting Options object
            //WeatherAPIOptions options = new WeatherAPIOptions();
            //_configuration.GetSection("weatherapi").Bind(options);
            //ViewBag.ClientID = options.ClientId;
            //ViewBag.ClientSecret = options.ClientSecret;

            //IOptions
            ViewBag.ClientID = _weatherAPIOptions.ClientId;
            ViewBag.ClientSecret = _weatherAPIOptions.ClientSecret;
            return View();
        }
    }
}
