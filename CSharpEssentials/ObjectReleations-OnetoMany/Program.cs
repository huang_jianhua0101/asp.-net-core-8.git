﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectReleations_OnetoMany
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student();
            student.RollNo = 1;
            student.StudnetName = "Allen";
            student.Email = "allen@example.com";
            student.Examinations = new List<Examination>();
            student.Examinations.Add(new Examination()
            {
                ExaminationName = "Module Test1",
                Month = 5,
                Year = 2024,
                MaxMarks = 100,
                SecuredMarks = 87
            });
            student.Examinations.Add(new Examination()
            {
                ExaminationName = "Module Test2",
                Month = 6,
                Year = 2024,
                MaxMarks = 100,
                SecuredMarks = 60
            });
            student.Print();
            Console.ReadKey();
        }
    }
}
