﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectReleations_OnetoMany
{
    public class Examination
    {
        public string ExaminationName { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }   

        public int MaxMarks { get; set; }

        public int SecuredMarks { get; set; }

        public override string ToString()
        {
            return $"ExaminationName: {ExaminationName}\r\nMonth: {Month}\r\nYear: {Year}\r\nMaxMarks: {MaxMarks}\r\nSecuredMarks: {SecuredMarks}";
        }
    }
}
