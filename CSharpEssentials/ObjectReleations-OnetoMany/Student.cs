﻿using College;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectReleations_OnetoMany
{
    public class Student
    {
        public int RollNo { get; set; }

        public string StudnetName { get; set; }

        public string Email { get; set; }

        public List<Examination> Examinations { get; set; } 

        public override string ToString()
        {
            return $"RollNo: {RollNo}\r\nStudentName: {StudnetName}\r\nEmail: {Email}\r\n";
        }

        public void Print()
        {
            Console.WriteLine(this.ToString());
            foreach (var examination in Examinations)
            {
                Console.WriteLine(examination.ToString());
            }
        }
    }
}
