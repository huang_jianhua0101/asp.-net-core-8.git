﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionofObjects
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Product> products = new List<Product>();
            string choice;
            do
            {
                Console.WriteLine("Enter Product ID:");
                int pid = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter Product Name:");
                string pname = Console.ReadLine();
                Console.WriteLine("Enter Price:");
                double unitPrice = double.Parse(Console.ReadLine());
                Console.WriteLine("Enter Date Of Manufacture(yyyy-MM-dd):");
                DateTime dom = DateTime.Parse(Console.ReadLine());

                Product product = new Product()
                {
                    ProductID = pid,
                    ProductName = pname,
                    Price = unitPrice,
                    DateOfManufacture = dom
                };

                products.Add(product);
                Console.WriteLine("Product Added");
                Console.WriteLine();
                Console.WriteLine("Continue(Y/N)?");
                choice = Console.ReadLine();
                Console.WriteLine();

            } while (choice.ToUpper() != "NO" && choice.ToUpper() != "N");

            //Print all products
            foreach (Product product in products)
            {
                Console.WriteLine(product.ToString());
            }
            Console.ReadKey();
        }
    }
}
