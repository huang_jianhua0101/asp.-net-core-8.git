﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Dictionary
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //create an empty dictionary
            Dictionary<int, string> employees = new Dictionary<int, string>()
            {
                {101,"Scott" },
                {102,"Smith" },
                {103,"Allen" },
            };

            //Remove
            employees.Remove(102);

            //foreach loop for dictionary
            foreach (KeyValuePair<int,string> item in employees)
            {
                Console.WriteLine(item.Key + ", " + item.Value);
            }

            //get value based on the key
            string s = employees[101];
            Console.WriteLine($"\nValue at 101: {s}");

            //Keys
            Dictionary<int,string>.KeyCollection keys = employees.Keys;
            foreach (int item in keys)
            {
                Console.WriteLine(item);
            }

            //Duplicate key: exception
            //employees.Add(101, "");

            //ContainsKey
            bool b = employees.ContainsKey(103);
            Console.WriteLine(b);

            //ContainsValue
            b = employees.ContainsValue("Scott");
            Console.WriteLine(b);

            //Clear
            employees.Clear();

            Console.ReadKey();
        }
    }
}
