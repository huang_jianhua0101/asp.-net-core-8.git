﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;

namespace EventExample
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //create obj of Subscriber class
            Subscriber subscriber = new Subscriber();

            //create obj of Publisher class
            Publisher publisher = new Publisher();

            //handle the event(or) subscribe to event
            publisher.myEvent += subscriber.Add;
            publisher.myEvent += delegate (int a, int b)
            {
                Console.WriteLine(a - b);
            };
            publisher.myEvent += (a, b) =>
            {
                Console.WriteLine(a * b);
            };
            //invoke the event
            publisher.RaiseEvent(10, 20);

            Console.ReadKey();
        }
    }
}
