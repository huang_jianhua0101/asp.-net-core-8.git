﻿using College;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectReleations
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Student class's object
            Student student = new Student();
            student.RollNo = 123;
            student.StudnetName = "Scott";
            student.Email = "scott@gmail.com";

            //Branch class's object
            Branch br = new Branch();
            br.BranchName = "Computer Science Engineering";
            br.NoOfSemesters = 8;

            //one-to-one releation
            student.Branch = br;

            Console.WriteLine(student.ToString());
            Console.ReadKey();
        }
    }
}
