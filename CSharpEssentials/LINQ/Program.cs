﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    class Employee
    {
        public int EmpID { get; set; }
        public string EmpName { get; set; }
        public string Job { get; set; }
        public string City { get; set; }
        public double Salary { get; set; }

        public override string ToString()
        {
            return $"EmpID: {EmpID}\r\nEmpName: {EmpName}\r\nJob: {Job}\r\nCity: {City}\r\n";
        }
    }


    internal class Program
    {
        static void Main(string[] args)
        {
            List<Employee> employees = new List<Employee>()
            {
                new Employee(){EmpID = 101, EmpName = "Henry", Job = "Designer", City = "Boston", Salary = 7232},
                new Employee(){EmpID = 102, EmpName = "Jack", Job = "Developer", City = "New York", Salary = 4500},
                new Employee(){EmpID = 103, EmpName = "Gabriel", Job = "Analyst", City = "Tokyo", Salary = 1293},
                new Employee(){EmpID = 104, EmpName = "William", Job = "Manager", City = "Tokyo", Salary = 2873},
                new Employee(){EmpID = 105, EmpName = "Alexa", Job = "Manager", City = "New York", Salary = 6232},
                new Employee(){EmpID = 106, EmpName = "Jessica", Job = "Manager", City = "New York", Salary = 5000},
            };

            //Min,Max,Sum,Average,Count
            var result1 = employees.Min(e => e.Salary);
            Console.WriteLine("Min:" + result1.ToString());
            var result2 = employees.Max(e => e.Salary);
            Console.WriteLine("Max:" + result2.ToString());
            var result3 = employees.Sum(e => e.Salary);
            Console.WriteLine("Sum:" + result3.ToString());
            var result4 = employees.Average(e => e.Salary);
            Console.WriteLine("Average:" + result4.ToString());
            var result5 = employees.Count();
            Console.WriteLine("Count:" + result5.ToString());

            Console.ReadKey();
        }
    }
}
