﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectReleations_ManytoOne
{
    public class Employee
    {
        public int EmployeeID {  get; set; }
        public string EmployeeName {  get; set; }
        public string Email { get; set; }
        public Department Department { get; set; }

        public override string ToString()
        {
            return $"EmployeeID: {EmployeeID}\r\nEmployeeName: {EmployeeName}\r\nEmail: {Email}";
        }

        public void Print()
        {
            Console.WriteLine(this.ToString());
            Console.WriteLine(Department.ToString());
        }
    }
}
