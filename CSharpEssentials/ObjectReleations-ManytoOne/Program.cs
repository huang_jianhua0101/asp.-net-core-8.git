﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectReleations_ManytoOne
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Employee employee1 = new Employee() { EmployeeID = 1, EmployeeName = "Scott", Email = "Scott@example.com" };
            Employee employee2 = new Employee() { EmployeeID = 2, EmployeeName = "Allen", Email = "Allen@example.com" };
            Employee employee3 = new Employee() { EmployeeID = 3, EmployeeName = "Smith", Email = "Smith@example.com" };
            
            Department department = new Department() { DepartmentID = 10, DepartmentName = "Accounting"};
            employee1.Department = department;
            employee2.Department = department;
            employee3.Department = department;

            employee1.Print();
            employee2.Print();
            employee3.Print();
            Console.ReadKey();
        }
    }
}
