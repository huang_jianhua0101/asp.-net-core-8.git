﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectReleations_ManytoOne
{
    public class Department
    {
        public int DepartmentID { get; set; }   
        public string DepartmentName { get; set; }

        public override string ToString()
        {
            return $"DepartmentID: {DepartmentID}\r\nDepartmentName: {DepartmentName}\r\n";
        }
    }
}
