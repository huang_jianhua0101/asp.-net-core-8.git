﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace College
{
    public class Student
    {
        public int RollNo { get; set; } 

        public string StudnetName { get; set; }

        public string Email { get; set; }  

        public Branch Branch { get; set; } //contains reference to object of Branch class, that represents the branch that the current student belongs to.

        public override string ToString()
        {
            return $"RollNo: {RollNo}\r\nStudentName: {StudnetName}\r\nEmail: {Email}\r\nBranchName: {Branch.BranchName}\r\nNoOfSemesters:{Branch.NoOfSemesters}\r\n";
        }
    }
}
