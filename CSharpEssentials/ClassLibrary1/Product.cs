﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


namespace ClassLibrary1
{
    public class Product
    {
        /// <summary>
        /// Represents a Product in ECommerce application
        /// </summary>
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public double Price { get; set; }
        public DateTime DateOfManufacture { get; set; }

        public override string ToString()
        {
            return $"ProductID: {ProductID}, ProductName: {ProductName}, Price: {Price}, Date Of Manufacture: {DateOfManufacture}";
        }
    }
}
