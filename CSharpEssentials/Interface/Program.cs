﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Manager manager = new Manager(800,"Scott","SH");
            SalesMan salesMan = new SalesMan(1001, "KK", "BJ");
            Console.WriteLine(manager.GetHealthInsuranceAmount());
            Console.WriteLine(salesMan.GetHealthInsuranceAmount());
            Console.ReadKey();
        }
    }
}
