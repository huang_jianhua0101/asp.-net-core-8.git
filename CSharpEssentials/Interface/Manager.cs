﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    public class Manager : IEmployee
    {
        private int _empID;
        private string _empName;
        private string _location;

        public int EmpID
        {
            get
            {
                return _empID;
            }
            set
            {
                if (value <= 1000) 
                {
                    _empID = value;
                }
            }
        }
        public string EmpName
        {
            get
            {
                return _empName;
            }
            set
            {
                _empName = value;
            }
        }
        public string Location
        {
            get
            {
                return _location;
            }
            set
            {
                _location = value;
            }
        }

        public Manager(int empID, string empName, string location)
        {
            _empID = empID;
            _empName = empName;
            _location = location;
        }

        public string GetHealthInsuranceAmount()
        {
            return "Manager's Health Insurance Amount is: 1000";
        }
    }
}
