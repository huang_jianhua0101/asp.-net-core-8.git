﻿class Employee
{
    public string Name;
    public int x;
}

class EmployeeBusinesLogic
{
    public Employee? GetEmployee()
    {
        return null;
    }
}


internal class Program
{
    static void Main(string[] args)
    {
        Employee? employee = new EmployeeBusinesLogic().GetEmployee();
        Console.WriteLine(employee?.x);
    }
}