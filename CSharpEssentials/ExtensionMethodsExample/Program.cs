﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionMethodsExample
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Product product = new Product()
            {
                ProductCost = 1000,
                DiscountPercentage = 80
            };
            Console.WriteLine(product.GetDiscount());
            Console.ReadKey();
        }
    }
}
