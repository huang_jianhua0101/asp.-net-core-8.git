using MinimalAPI.Models;
using MinimalAPI.RouteGroups;
using System.ComponentModel.Design;
using System.Text.Json;

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();


//MapGroup
var mapGroup = app.MapGroup("/products").ProductsAPI();


app.Run();
