﻿using Microsoft.AspNetCore.Mvc;
using MinimalAPI.EndpointFilters;
using MinimalAPI.Models;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Text.Json;

namespace MinimalAPI.RouteGroups
{
    public static class ProductsMapGroup
    {
        private static List<Product> products = new List<Product>()
        {
            new Product()
            {
                Id = 1,
                ProductName = "Smart Phone",
            },
            new Product()
            {
                Id= 2,
                ProductName = "Smart TV"
            }
        };

        public static RouteGroupBuilder ProductsAPI(this RouteGroupBuilder group)
        {
            //GET /products
            group.MapGet("/", async (HttpContext context) =>
            {
                var content = string.Join('\n', products.Select(p => p.ToString()));
                await context.Response.WriteAsync(content);
            });

            //GET /products/{id}
            group.MapGet("/{id:int}", async (HttpContext context, int id) =>
            {
                Product? product = products.FirstOrDefault(p => p.Id == id);
                if (product == null)
                {
                    context.Response.StatusCode = StatusCodes.Status400BadRequest;
                    await context.Response.WriteAsync("Incorrect Product Id");
                }
                else
                {
                    await context.Response.WriteAsync(JsonSerializer.Serialize(product));
                }
            });

            //POST /products
            group.MapPost("/", async (HttpContext context, Product product) =>
            {
                products.Add(product);
                await context.Response.WriteAsync("Product Added");
                await context.Response.WriteAsync(JsonSerializer.Serialize(product));
            }).AddEndpointFilter<CustomEndpointFilter>()
                .AddEndpointFilter(async (EndpointFilterInvocationContext context, EndpointFilterDelegate next) =>
            {
                //Before logic
                var product = context.Arguments.OfType<Product>().FirstOrDefault();
                if (product == null)
                {
                    return Results.BadRequest("Product details are not found in the request");
                }

                var validationContext = new ValidationContext(product);
                List<ValidationResult> errors = new List<ValidationResult>();
                bool isValid = Validator.TryValidateObject(product, validationContext, errors, true);
                if (!isValid)
                {
                    return Results.BadRequest(errors.FirstOrDefault()?.ErrorMessage);
                }

                //invokes the subsequent endpoint filter or endpoint's request delegate
                var result = await next(context);

                //After logic


                return result;
            });


            //PUT /products/{id}
            group.MapPut("/{id}", async (HttpContext context, int id, [FromBody] Product product) =>
            {
                Product? productGet = products.FirstOrDefault(p => p.Id == id);
                if (productGet == null)
                {
                    context.Response.StatusCode = StatusCodes.Status400BadRequest;
                    await context.Response.WriteAsync("Incorrect Product Id");
                }
                else
                {
                    productGet.ProductName = product.ProductName;
                    await context.Response.WriteAsJsonAsync(product);
                    await context.Response.WriteAsync("Product Updated");
                }
            });

            //DELETE /products/{id}
            group.MapDelete("/{id}", async (HttpContext context, int id) =>
            {
                Product? product = products.FirstOrDefault(p => p.Id == id);
                if (product == null)
                {
                    //context.Response.StatusCode = StatusCodes.Status400BadRequest;hi
                    //await context.Response.WriteAsync("Incorrect Product Id");
                    //return Results.BadRequest(new { error = "Incorrect Product Id" });
                    return Results.ValidationProblem(new Dictionary<string, string[]>
                    {
                        { "id", new string[] { "Incorrect Product Id"} },
                        { "test", new string[] { "test"} }
                    });
                }
                else
                {
                    products.Remove(product);
                    //await context.Response.WriteAsJsonAsync(product);
                    //await context.Response.WriteAsync("Product Deleted");
                    return Results.Ok(new { message = "Product Deleted" });
                }
            });

            return group;
        }
    }
}
