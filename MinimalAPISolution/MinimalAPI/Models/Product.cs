﻿using System.ComponentModel.DataAnnotations;

namespace MinimalAPI.Models
{
    public class Product
    {
        [Required(ErrorMessage = "Id can't be blank")]
        [Range(1, int.MaxValue, ErrorMessage = "Id should be between 1 to maximum value of int")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Product Name can't be blank")]
        public string? ProductName { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}, ProductName: {ProductName}";
        }
    }
}
