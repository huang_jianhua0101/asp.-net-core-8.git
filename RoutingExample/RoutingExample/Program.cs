using RoutingExample.CustomRouteConstraints;
using System.Security.Cryptography.Xml;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddRouting(options =>
{
    options.ConstraintMap.Add("months", typeof(MonthsCustomRouteConstraint));
});

var app = builder.Build();

//app.MapGet("/", () => "Hello World!");

#region
//app.Use(async (context, next) =>
//{
//    Endpoint? e1 = context.GetEndpoint();
//    if (e1 != null)
//    {
//        await context.Response.WriteAsync($"Endpoint1: {e1?.DisplayName}\r\n");
//    }
//    await next(context);
//});
#endregion

//enable routing
app.UseRouting();

#region
//app.Use(async (context, next) =>
//{
//    Endpoint? e1 = context.GetEndpoint();
//    if (e1 != null)
//    {
//        await context.Response.WriteAsync($"Endpoint2: {e1?.DisplayName}\r\n");
//    }
//    await next(context);
//});
#endregion

#region
////creating end points
//app.UseEndpoints(endpoints =>
//{
//    //add your end points
//    //Map方法可以处理HTTP GET/POST等所有方法
//    //MapGet只能处理HTTP GET
//    //MapPost只能处理HTTP POST
//    endpoints.Map("map1", async (context) =>
//    {
//        await context.Response.WriteAsync("In Map 1");
//    });
//    endpoints.Map("map2", async (context) =>
//    {
//        await context.Response.WriteAsync("In Map 2");
//    });
//    endpoints.MapGet("mapGet", async (context) =>
//    {
//        await context.Response.WriteAsync("In Map Get");
//    });
//    endpoints.MapPost("mapPost", async (context) =>
//    {
//        await context.Response.WriteAsync("In Map Post");
//    });
//});
#endregion

app.UseEndpoints(endpoints =>
{
    endpoints.Map("files/{filename}.{extension}", async context =>
    {
        string filename = Convert.ToString(context.Request.RouteValues["filename"]);
        string extension = Convert.ToString(context.Request.RouteValues["extension"]);
        await context.Response.WriteAsync($"In files: {filename} - {extension}\r\n");
        await context.Response.WriteAsync(context.GetEndpoint().DisplayName + "\r\n");
    });
    endpoints.Map("employee/profile/{EmployeeName:length(3,7):alpha=Jianhua}", async context =>
    {
        string employeeName = Convert.ToString(context.Request.RouteValues["EmployeeName"]);
        await context.Response.WriteAsync($"In employee: {employeeName}\r\n");
        await context.Response.WriteAsync(context.GetEndpoint().DisplayName + "\r\n");
    });
    endpoints.Map("products/details/{id:int?}", async context =>
    {
        int id = Convert.ToInt32(context.Request.RouteValues["id"]);
        await context.Response.WriteAsync($"In products: {id}\r\n");
        await context.Response.WriteAsync(context.GetEndpoint().DisplayName + "\r\n");
    });
    //Eg: daily-digest-report/{reportdate}
    endpoints.Map("daily-digest-report/{reportdate:datetime}", async context =>
    {
        DateTime reportdate = Convert.ToDateTime(context.Request.RouteValues["reportdate"]);
        await context.Response.WriteAsync($"In daily-digest-report-{reportdate.ToShortDateString()}");
    });
    //Eg: cities/cityid
    endpoints.Map("cities/{cityid:guid}", async context =>
    {
        Guid cityid = Guid.Parse(Convert.ToString(context.Request.RouteValues["cityid"]));
        await context.Response.WriteAsync($"City information - {cityid}");
    });
    //Eg: sales-report/2030/apr
    endpoints.Map("sales-report/{year:int:min(1900)}/{month:months}", async context =>
    {
        int year = Convert.ToInt32(context.Request.RouteValues["year"]);
        string? month = Convert.ToString(context.Request.RouteValues["month"]);
        if (true)
        {
            //Do something
        }
        else
        {
            //Do something
        }
        await context.Response.WriteAsync($"sales report - {year} - {month}");
    });

    //Eg: sales-report/2024/jan
    endpoints.Map("sales-report/2024/jan", async context =>
    {
        await context.Response.WriteAsync($"sales report - sales-report/2024/jan");
    });
});

//如果上面都不符合条件，那么执行这里的Run
app.Run(async context =>
{
    await context.Response.WriteAsync($"No route matched at {context.Request.Path}");
});

app.Run();
