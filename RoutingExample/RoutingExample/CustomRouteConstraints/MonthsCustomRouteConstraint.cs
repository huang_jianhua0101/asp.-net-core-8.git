﻿
using System.Text.RegularExpressions;

namespace RoutingExample.CustomRouteConstraints
{
    public class MonthsCustomRouteConstraint : IRouteConstraint
    {
        //"sales-report/{year:int:min(1900)}/{month:regex(^(apr|jul|oct|jan)$)}"
        //httpContext相当于中间件中的context
        //route被应用此路由约束的路由
        //routeKey相当于上面例子中的year/name中某个路由参数
        //values包含了路由中所有参数
        //routeDirection当有URL生成的时候是否需要检查收到的路由
        public bool Match(HttpContext? httpContext, IRouter? route, string routeKey, RouteValueDictionary values, RouteDirection routeDirection)
        {
            //check weather the value exists
            if (!values.ContainsKey(routeKey))  //month
            {
                return false; //not a match
            }

            Regex regex = new Regex("^(apr|jul|oct|jan)$");
            string? monthValue = Convert.ToString(values[routeKey]);
            if (regex.IsMatch(monthValue))
            {
                return true; //it's a match
            }
            else
            {
                return false; //not a match
            }
        }
    }
}
