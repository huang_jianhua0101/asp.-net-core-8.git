﻿using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using ViewComponentsExample.Models;

namespace ViewComponentsExample.ViewComponents
{
    [ViewComponent]
    public class GridViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(PersonGridModel grid)
        {
            //PersonGridModel model = new PersonGridModel()
            //{
            //    GridTitle = "Person List",
            //    Persons = new List<Person>()
            //    {
            //        new Person(){PersonName = "KK", JobTitle = "Manager"},
            //        new Person(){PersonName = "LL", JobTitle = "Manager"},
            //        new Person(){PersonName = "SS", JobTitle = "Clerk"},
            //    }
            //};
            //ViewData["Grid"] = model;
            //return View("Sample", model); //invoked a partial view Views/Shared/Components/Grid/Deafult.cshtml
            return View("Sample", grid);
        }
    }
}
