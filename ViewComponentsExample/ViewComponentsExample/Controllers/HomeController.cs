﻿using Microsoft.AspNetCore.Mvc;
using ViewComponentsExample.Models;

namespace ViewComponentsExample.Controllers
{
    public class HomeController : Controller
    {
        [Route("/")]
        public IActionResult Index()
        {
            return View();
        }

        [Route("about")]
        public IActionResult About()
        {
            return View();
        }

        [Route("friends-list")]
        public IActionResult LoadFriendsList()
        {
            PersonGridModel model2 = new PersonGridModel()
            {
                GridTitle = "Friends",
                Persons = new List<Person>()
                {
                    new Person(){PersonName = "FAA", JobTitle = "F11"},
                    new Person(){PersonName = "FBB", JobTitle = "F22"},
                    new Person(){PersonName = "FCC", JobTitle = "F33"},
                }
            };
            return ViewComponent("Grid", new {grid = model2});
        }
    }
}
