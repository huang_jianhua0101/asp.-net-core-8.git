﻿using Microsoft.AspNetCore.Mvc;
using ViewsExample.Models;

namespace ViewsExample.Controllers
{
    public class HomeController : Controller
    {
        [Route("home")]
        [Route("/")]
        public IActionResult Index()
        {
            ViewData["appTitle"] = "Asp.Net Core Demo App";

            List<Person> people = new List<Person>()
            {
                new Person() {Name = "KK", BirthDate = DateTime.Parse("2000-01-02"), Gender = Gender.Male},
                new Person() {Name = "LL", BirthDate = DateTime.Parse("2000-01-06"), Gender = Gender.Female},
                new Person() {Name = "SS", BirthDate = null, Gender = Gender.Other},
            };
            ViewData["people"] = people;

            ViewBag.appTitle = "Asp.Net Core Demo App";
            ViewBag.people = people;
            //return View(); //Views/Home/Index.cshtml
            return View("Index2",people);
            //return View("abc"); //abc.cshtml
            //return new ViewResult() { ViewName = "abc" };
        }

        [Route("person-details/{name}")]
        public IActionResult Details(string? name)
        {
            if (name == null)
            {
                return Content("Person name can't be null");
            }

            List<Person> people = new List<Person>()
            {
                new Person() {Name = "KK", BirthDate = DateTime.Parse("2000-01-02"), Gender = Gender.Male},
                new Person() {Name = "LL", BirthDate = DateTime.Parse("2000-01-06"), Gender = Gender.Female},
                new Person() {Name = "SS", BirthDate = null, Gender = Gender.Other},
            };

            Person? matchingPerson = people.Where(temp => temp.Name.ToUpper() == name.ToUpper()).FirstOrDefault();

            return View(matchingPerson);
        }

        [Route("person-with-product")]
        public IActionResult PersonWithProduct()
        {
            Person person = new Person() { Name = "ZZ", BirthDate = DateTime.Parse("2000-01-02"), Gender = Gender.Male };
            Product product = new Product() { ProductId = 1, ProductName = "Air Conditioner" };
            PersonAndProductWrapperModel personAndProductWrapperModel = 
                new PersonAndProductWrapperModel() { PersonData = person, ProductData = product };
            return View(personAndProductWrapperModel);
        }

        [Route("home/all")]
        public IActionResult All()
        {
            return View();
            //Views/Home/All.cshtml
            //Views/Shared/All.cshtml
        }
    }
}
