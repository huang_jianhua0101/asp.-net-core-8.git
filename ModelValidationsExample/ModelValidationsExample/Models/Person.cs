﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using ModelValidationsExample.CustomValidators;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace ModelValidationsExample.Models
{
    public class Person : IValidatableObject
    {
        public Guid PersonId { get; set; }

        [Required(ErrorMessage = "{0} can't be empty or null!\n")] //只能用0代替一个属性名，不能用1和2代替多个属性名
        [Display(Name = "Person Name")] //用Person Name显示名替代PersonName属性
        [StringLength(40, MinimumLength = 3, ErrorMessage = "{0} should be between {2} and {1} characters long")]
        [RegularExpression("^[A-Za-z0-9_. ]+$", ErrorMessage = "{0} should contain only alphabets, space and dot (.)")]
        public string? PersonName { get; set; }

        [Required(ErrorMessage = "{0} can't be blank")]
        [EmailAddress(ErrorMessage = "{0} should be a proper email address")]
        public string? Email { get; set; }

        [Phone()]
        //[ValidateNever]
        public string? Phone { get; set; }

        [Required(ErrorMessage = "{0} can't be blank")]
        public string? Password { get; set; }

        [Required(ErrorMessage = "{0} can't be blank")]
        [Compare("Password",ErrorMessage = "{0} and {1} do not match")]
        [Display(Name = "Re-enter Password")]
        public string? ConfirmPassword { get; set; }

        [Range(0,999.99,ErrorMessage = "{0} should be between ￥{1} and ￥{2}\n")]
        public double? Price { get; set; }

        [MiniYearValidation]
        [BindNever]
        public DateTime? DateOfBirth { get; set; }

        public DateTime? FromDate { get; set; }
        [DateRangeValidation("FromDate")]
        public DateTime? ToDate { get; set; }

        public int? Age { get; set; }

        public List<string?> Tags { get; set; } = new List<string?>();

        public override string ToString()
        {
            return $"Person object - PersonName: {PersonName}, Email: {Email}, " +
                $"Phone: {Phone}, Password: {Password}, ConfirmPassword: {ConfirmPassword}, Price: {Price} ";
        }

        //只有上面的Validation Attribute都验证完没有问题，下面的程序才会执行
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (DateOfBirth.HasValue == false && Age.HasValue == false)
            {
                yield return new ValidationResult("Either of Date of Birth or Age must be supplied",
                    new[] {nameof(Age)});
            }
        }
    }
}
