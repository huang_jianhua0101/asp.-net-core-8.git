﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using ModelValidationsExample.Models;

namespace ModelValidationsExample.CustomModelBinders
{
    public class PersonModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            Person person = new Person();
            if (bindingContext.ValueProvider.GetValue("FirstName").Count() > 0
                && bindingContext.ValueProvider.GetValue("LastName").Length > 0
                && bindingContext.ValueProvider.GetValue("Email").Length > 0)
            {
                person.PersonName = string.Concat(bindingContext.ValueProvider.GetValue("FirstName").FirstValue,
                        " ", bindingContext.ValueProvider.GetValue("LastName").FirstValue);
                person.Email = bindingContext.ValueProvider.GetValue("Email").FirstValue;
            }
            
            bindingContext.Result = ModelBindingResult.Success(person);
            return Task.CompletedTask;
        }
    }
}
