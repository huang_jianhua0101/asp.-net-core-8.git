﻿using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace ModelValidationsExample.CustomValidators
{
    public class DateRangeValidationAttribute : ValidationAttribute
    {
        public string OtherPropName { get; set; }

        public DateRangeValidationAttribute() 
        { 

        }

        public DateRangeValidationAttribute(string otherPropName)
        {
            this.OtherPropName = otherPropName;
        }

        protected override ValidationResult? IsValid(object? value, 
            ValidationContext validationContext)
        {
            if (value != null)
            {
                //get to_date
                DateTime to_date = Convert.ToDateTime(value);

                //get from_date
                PropertyInfo? propertyInfo = validationContext.ObjectType.GetProperty(OtherPropName);
                if (propertyInfo != null)
                {
                    DateTime from_date = Convert.ToDateTime(propertyInfo.GetValue(validationContext.ObjectInstance, null));

                    if (from_date > to_date)
                    {
                        return new ValidationResult(ErrorMessage,
                            new string[] { OtherPropName, validationContext.MemberName });
                    }
                    else
                    {
                        return ValidationResult.Success;
                    }
                }
                return null;
            }
            return null;
        }
    }
}
