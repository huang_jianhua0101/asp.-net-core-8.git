﻿using System.ComponentModel.DataAnnotations;

namespace ModelValidationsExample.CustomValidators
{
    public class MiniYearValidationAttribute : ValidationAttribute
    {
        public int MiniYear { get; set; } = DateTime.Now.Year;  //设置默认值

        public string DefaultErrorMessage { get; set; } = "Year should not be greater than {0}";

        public MiniYearValidationAttribute() 
        { 

        }

        public MiniYearValidationAttribute(int miniYear)
        {
            this.MiniYear = miniYear;
        }

        protected override ValidationResult? IsValid(object? value, 
            ValidationContext validationContext)
        {
            if (value != null)
            {
                DateTime date = (DateTime)value;
                if (date.Year > MiniYear)
                {
                    return new ValidationResult(string.Format(ErrorMessage ?? DefaultErrorMessage, MiniYear));
                    //return new ValidationResult($"Mini year allowed is {MiniYear}");
                }
                else
                {
                    return ValidationResult.Success;
                }
            }
            return null;
        }
    }
}
