﻿using Entities;
using ServiceContracts.DTO;
using ServiceContracts;
using CsvHelper;
using System.Globalization;
using CsvHelper.Configuration;
using OfficeOpenXml;
using RepositoryContracts;
using Microsoft.Extensions.Logging;
using Serilog;
using SerilogTimings;

namespace Services
{
    public class PersonsGetterService : IPersonsGetterService
    {
        //private field
        private readonly IPersonsRepository _personsRepository;
        private readonly ILogger<PersonsGetterService> _logger;

        private readonly IDiagnosticContext _diagnosticContext;

        //constructor
        public PersonsGetterService(IPersonsRepository personsRepository, ILogger<PersonsGetterService> logger = null, IDiagnosticContext diagnosticContext = null)
        {
            _personsRepository = personsRepository;
            _logger = logger;
            _diagnosticContext = diagnosticContext;
        }


        public virtual async Task<List<PersonResponse>> GetAllPersons()
        {
            _logger.LogInformation("【PersonsService】- GetAllPersons of PersonsService");
            //SELECT * from Persons
            var persons = await _personsRepository.GetAllPersonsAsync();
            return persons
              .Select(temp => temp.ToPersonResponse()).ToList();
            //return _db.sp_GetAllPersons()
            //    .Select(p => ConvertPersonToPersonResponse(p)).ToList();
        }


        public virtual async Task<PersonResponse?> GetPersonByPersonId(Guid? PersonId)
        {
            if (PersonId == null)
                return null;

            Person? person = await _personsRepository.GetPersonByPersonIdAsync(PersonId.Value);

            if (person == null)
                return null;

            return person.ToPersonResponse();
        }

        public virtual async Task<List<PersonResponse>> GetFilteredPersons(string searchBy, string? searchString)
        {
            _logger.LogInformation("【PersonsService】- GetFilteredPersons of PersonsService");

            List<Person> persons = null;
            using (Operation.Time("Time for Filtered Persons from Database"))
            {
                persons = searchBy switch
                {
                    nameof(PersonResponse.PersonName) =>
                     (await _personsRepository.GetFilteredPersonsAsync(temp =>
                     temp.PersonName.Contains(searchString))).ToList(),

                    nameof(PersonResponse.Email) =>
                     (await _personsRepository.GetFilteredPersonsAsync(temp =>
                     temp.Email.Contains(searchString))).ToList(),

                    nameof(PersonResponse.DateOfBirth) =>
                     (await _personsRepository.GetFilteredPersonsAsync(temp =>
                     temp.DateOfBirth.Value.ToString("dd MMMM yyyy").Contains(searchString))).ToList(),


                    nameof(PersonResponse.Gender) =>
                     (await _personsRepository.GetFilteredPersonsAsync(temp =>
                     temp.Gender.Contains(searchString))).ToList(),

                    nameof(PersonResponse.CountryId) =>
                     (await _personsRepository.GetFilteredPersonsAsync(temp =>
                     temp.Country.CountryName.Contains(searchString))).ToList(),

                    nameof(PersonResponse.Address) =>
                    (await _personsRepository.GetFilteredPersonsAsync(temp =>
                    temp.Address.Contains(searchString))).ToList(),

                    _ => (await _personsRepository.GetAllPersonsAsync()).ToList()
                };
            }   //end of "using block" of serilog timings
            

            _diagnosticContext.Set("Persons", persons);

            return persons.Select(temp => temp.ToPersonResponse()).ToList();
        }



        public virtual async Task<MemoryStream> GetPersonsCSV()
        {
            MemoryStream memoryStream = new MemoryStream();
            StreamWriter streamWriter = new StreamWriter(memoryStream);

            CsvConfiguration csvConfiguration = new CsvConfiguration(CultureInfo.InvariantCulture);

            CsvWriter csvWriter = new CsvWriter(streamWriter, csvConfiguration, true);

            csvWriter.WriteField(nameof(PersonResponse.PersonName));
            csvWriter.WriteField(nameof(PersonResponse.Email));
            csvWriter.WriteField(nameof(PersonResponse.DateOfBirth));
            csvWriter.WriteField(nameof(PersonResponse.Age));
            csvWriter.WriteField(nameof(PersonResponse.Gender));
            csvWriter.WriteField(nameof(PersonResponse.Country));
            csvWriter.WriteField(nameof(PersonResponse.Address));
            csvWriter.WriteField(nameof(PersonResponse.ReceiveNewsLetters));
            await csvWriter.NextRecordAsync();

            List<PersonResponse> persons = (await _personsRepository.GetAllPersonsAsync())
                .Select(p => p.ToPersonResponse()).ToList();

            foreach (PersonResponse person in persons)
            {
                csvWriter.WriteField(person.PersonName);
                csvWriter.WriteField(person.Email);
                if (person.DateOfBirth.HasValue)
                {
                    csvWriter.WriteField(person.DateOfBirth.Value.ToString("yyyy-MM-dd"));
                }
                else
                {
                    csvWriter.WriteField("");
                }
                csvWriter.WriteField(person.Age);
                csvWriter.WriteField(person.Gender);
                csvWriter.WriteField(person.Country);
                csvWriter.WriteField(person.Address);
                csvWriter.WriteField(person.ReceiveNewsLetters);
                await csvWriter.NextRecordAsync();
                await csvWriter.FlushAsync();
            }

            memoryStream.Position = 0;
            return memoryStream;
        }

        public virtual async Task<MemoryStream> GetPersonsExcel()
        {
            MemoryStream memoryStream = new MemoryStream();
            using (ExcelPackage excelPackage = new ExcelPackage(memoryStream))
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("PersonsSheet");
                worksheet.Cells["A1"].Value = nameof(PersonResponse.PersonName);
                worksheet.Cells["B1"].Value = nameof(PersonResponse.Email);
                worksheet.Cells["C1"].Value = nameof(PersonResponse.DateOfBirth);
                worksheet.Cells["D1"].Value = nameof(PersonResponse.Age);
                worksheet.Cells["E1"].Value = nameof(PersonResponse.Gender);
                worksheet.Cells["F1"].Value = nameof(PersonResponse.Country);
                worksheet.Cells["G1"].Value = nameof(PersonResponse.Address);
                worksheet.Cells["H1"].Value = nameof(PersonResponse.ReceiveNewsLetters);

                using (ExcelRange headerCells = worksheet.Cells["A1:H1"])
                {
                    headerCells.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    headerCells.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);
                    headerCells.Style.Font.Bold = true;
                }

                int row = 2;
                List<PersonResponse> persons = (await _personsRepository.GetAllPersonsAsync())
                .Select(p => p.ToPersonResponse()).ToList();
                foreach (PersonResponse person in persons)
                {
                    worksheet.Cells[row, 1].Value = person.PersonName;
                    worksheet.Cells[row, 2].Value = person.Email;
                    if (person.DateOfBirth.HasValue)
                    {
                        worksheet.Cells[row, 3].Value = person.DateOfBirth.Value.ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        worksheet.Cells[row, 3].Value = "";
                    }
                    worksheet.Cells[row, 4].Value = person.Age;
                    worksheet.Cells[row, 5].Value = person.Gender;
                    worksheet.Cells[row, 6].Value = person.Country;
                    worksheet.Cells[row, 7].Value = person.Address;
                    worksheet.Cells[row, 8].Value = person.ReceiveNewsLetters;
                    row++;
                }

                worksheet.Cells[$"A1:H{row}"].AutoFitColumns();

                await excelPackage.SaveAsync();
            }
            memoryStream.Position = 0;
            return memoryStream;
        }
    }
}