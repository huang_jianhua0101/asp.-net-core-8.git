﻿using System;
using Entities;
using ServiceContracts.DTO;
using ServiceContracts;
using Services.Helpers;
using ServiceContracts.Enums;
using Microsoft.EntityFrameworkCore;
using CsvHelper;
using System.Globalization;
using CsvHelper.Configuration;
using System.Reflection;
using OfficeOpenXml;
using System.ComponentModel.DataAnnotations.Schema;
using RepositoryContracts;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Serilog;
using SerilogTimings;
using Exceptions;

namespace Services
{
    public class PersonsUpdaterService : IPersonsUpdaterService
    {
        //private field
        private readonly IPersonsRepository _personsRepository;
        private readonly ILogger<PersonsUpdaterService> _logger;

        private readonly IDiagnosticContext _diagnosticContext;

        //constructor
        public PersonsUpdaterService(IPersonsRepository personsRepository, ILogger<PersonsUpdaterService> logger = null, IDiagnosticContext diagnosticContext = null)
        {
            _personsRepository = personsRepository;
            _logger = logger;
            _diagnosticContext = diagnosticContext;
        }

        public async Task<PersonResponse> UpdatePerson(PersonUpdateRequest? personUpdateRequest)
        {
            if (personUpdateRequest == null)
                throw new ArgumentNullException(nameof(Person));

            //validation
            ValidationHelper.ModelValidation(personUpdateRequest);

            //get matching person object to update
            Person? matchingPerson = await _personsRepository.GetPersonByPersonIdAsync(personUpdateRequest.PersonId);
            if (matchingPerson == null)
            {
                //throw new ArgumentException("Given person id doesn't exist");
                throw new InvalidPersonIdException("Given person id doesn't exist");
            }

            //update all details
            await _personsRepository.UpdatePersonAsync(matchingPerson);

            return matchingPerson.ToPersonResponse();
        }
    }
}