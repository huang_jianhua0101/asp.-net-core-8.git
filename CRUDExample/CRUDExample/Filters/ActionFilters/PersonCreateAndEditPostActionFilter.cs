﻿using CRUDExample.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Rendering;
using ServiceContracts;
using ServiceContracts.DTO;

namespace CRUDExample.Filters.ActionFilters
{
    public class PersonCreateAndEditPostActionFilter : IAsyncActionFilter
    {
        private readonly ICountriesService _countriesService;
        private readonly ILogger<PersonCreateAndEditPostActionFilter> _logger;

        public PersonCreateAndEditPostActionFilter(ICountriesService countriesService, ILogger<PersonCreateAndEditPostActionFilter> logger)
        {
            _countriesService = countriesService;
            _logger = logger;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            //To DO: before logic
            if (context.Controller is PersonsController personsController)
            {
                if (!personsController.ModelState.IsValid)
                {
                    List<CountryResponse> countries = await _countriesService.GetAllCountries();
                    //ViewBag.Countries = countries;
                    personsController.ViewBag.Countries = countries.Select(c => new SelectListItem()
                    { Text = c.CountryName, Value = c.CountryId.ToString() });
                    personsController.ViewBag.Errors = personsController.ModelState.Values.SelectMany(v => v.Errors).Select(msg => msg.ErrorMessage);

                    var personAddRequest = context.ActionArguments["personAddRequest"];
                    context.Result = personsController.View(personAddRequest); //short-circuits or skips the subsequent action filters & action methods
                }
            }
            else
            {
                await next(); //invokes the subsequent action filters & action methods
            }

            _logger.LogInformation("In after logic of PersonCreateAndEdit Action filter");
        }
    }
}
