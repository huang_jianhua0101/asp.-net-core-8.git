﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace CRUDExample.Filters.ResultFilters
{
    public class PersonsAlwaysRunResultFilter : IAsyncAlwaysRunResultFilter
    {
        private readonly ILogger<PersonsAlwaysRunResultFilter> _logger;

        public PersonsAlwaysRunResultFilter(ILogger<PersonsAlwaysRunResultFilter> logger)
        {
            _logger = logger;
        }

        public async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
        {
            if (context.Filters.OfType<SkipFilter>().Any())
            {
                await next();
            }
            else
            {
                _logger.LogInformation("{FilterName}.{Method} before", nameof(PersonsAlwaysRunResultFilter), nameof(OnResultExecutionAsync));
                await next();
                _logger.LogInformation("{FilterName}.{Method} after", nameof(PersonsAlwaysRunResultFilter), nameof(OnResultExecutionAsync));
            }
        }
    }
}
