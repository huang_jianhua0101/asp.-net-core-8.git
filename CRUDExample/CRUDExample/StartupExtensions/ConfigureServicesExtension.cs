﻿using CRUDExample.Filters.ActionFilters;
using Entities;
using Microsoft.AspNetCore.HttpLogging;
using Microsoft.EntityFrameworkCore;
using Repositories;
using RepositoryContracts;
using ServiceContracts;
using Services;
using System.Xml.Serialization;

namespace CRUDExample
{
    public static class ConfigureServicesExtension
    {
        public static IServiceCollection ConfigureServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ResponseHeaderActionFilter>();

            //it adds controllers and views as services
            services.AddControllersWithViews(options =>
            {
                //options.Filters.Add<ResponseHeaderActionFilter>();    //无法传递参数

                var logger = services.BuildServiceProvider().GetRequiredService<ILogger<ResponseHeaderActionFilter>>();
                options.Filters.Add(new ResponseHeaderActionFilter(logger)
                {
                    Key = "MyKeyFromGlobal",
                    Value = "MyValueFromGlobal",
                    Order = 2
                });  //可以传递参数
                     //options.Filters.Add<PersonCreateAndEditPostActionFilter>();
            });

            //add services into IoC container
            services.AddScoped<ICountriesRepository, CountriesRepository>();
            services.AddScoped<IPersonsRepository, PersonsRepository>();

            services.AddScoped<ICountriesService, CountriesService>();
            //services.AddScoped<IPersonsGetterService, PersonsGetterService>();
            services.AddScoped<IPersonsGetterService, PersonsGetterServiceWithFewExcelFileds>();
            //services.AddScoped<IPersonsGetterService, PersonsGetterServiceChild>();
            services.AddScoped<PersonsGetterService, PersonsGetterService>();

            services.AddScoped<IPersonsUpdaterService, PersonsUpdaterService>();
            services.AddScoped<IPersonsSorterService, PersonsSorterService>();
            services.AddScoped<IPersonsDeleterService, PersonsDeleterService>();
            services.AddScoped<IPersonsAdderService, PersonsAdderService>();



            services.AddTransient<PersonsListActionFilter>();


            services.AddDbContext<ApplicationDbContext>
            (
                options =>
                {
                    string? connStr = configuration.GetConnectionString("DefaultConnection");
                    if (string.IsNullOrEmpty(connStr))
                    {
                        throw new ArgumentNullException("Connection String is empty!");
                    }
                    options.UseSqlServer(connStr);
                }
            );

            services.AddHttpLogging(options =>
            {
                options.LoggingFields = HttpLoggingFields.RequestPropertiesAndHeaders | HttpLoggingFields.ResponsePropertiesAndHeaders;
                options.CombineLogs = true;
            });

            return services;
        }
    }
}
