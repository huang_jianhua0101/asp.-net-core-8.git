﻿using AutoFixture;
using Entities;
using EntityFrameworkCoreMock;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Moq;
using RepositoryContracts;
using ServiceContracts;
using ServiceContracts.DTO;
using Services;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;


namespace CRUDxUnitTests
{
    public class CountriesServiceTest
    {
        //private fields
        private readonly IFixture _fixture;
        private readonly Mock<ICountriesRepository> _countriesRepositoryMock;
        private readonly ICountriesRepository _countriesRepository;
        private readonly ICountriesService _countriesService;
        
        public CountriesServiceTest()
        {
            _fixture = new Fixture();
            _countriesRepositoryMock = new Mock<ICountriesRepository>();
            _countriesRepository = _countriesRepositoryMock.Object;
            _countriesService = new CountriesService(_countriesRepository);
        }
        #region AddCountry
        //When CountryAddRequest is null, it should throw ArgumentNullException
        [Fact]
        public async Task AddCountry_NullCountry_ToBeArgumentNullException()
        {
            //Arrange
            CountryAddRequest? request = null;

            Country country = _fixture.Build<Country>()
                .With(c => c.Persons, null as List<Person>).Create();
            _countriesRepositoryMock.Setup(c => c.AddCountryAsync(It.IsAny<Country>()))
                .ReturnsAsync(country);
            //Act
            Func<Task> action = async () =>
            {
                await _countriesService.AddCountry(request);
            };

            //Assert
            await action.Should().ThrowAsync<ArgumentNullException>();
        }
        //When the CountryName is null , it should throw ArgumentException
        [Fact]
        public async Task AddCountry_CountryNameIsNull_ToBeArgumentException()
        {
            //Arrange
            CountryAddRequest request = _fixture.Build<CountryAddRequest>()
                .With(c => c.CountryName, null as string)
                .Create();
                
            Country country = _fixture.Build<Country>()
                .With(c => c.Persons, null as List<Person>)
                .Create();

            _countriesRepositoryMock.Setup(c => c.AddCountryAsync(It.IsAny<Country>()))
                .ReturnsAsync(country);
            //Act
            Func<Task> action = async () =>
            {
                await _countriesService.AddCountry(request);
            };
            //Assert
            await action.Should().ThrowAsync<ArgumentException>();
        }
        //When the CountryName is duplicate, it should throw ArgumentException
        [Fact]
        public async Task AddCountry_DuplicateCountryName_ToBeArgumentException()
        {
            //Arrange
            CountryAddRequest request1 = _fixture.Build<CountryAddRequest>()
                .With(c => c.CountryName, "USA").Create();
            CountryAddRequest request2 = _fixture.Build<CountryAddRequest>()
                .With(c => c.CountryName, "USA").Create();
            Country country1 = request1.ToCountry();
            Country country2 = request2.ToCountry();

            _countriesRepositoryMock.Setup(c => c.AddCountryAsync(It.IsAny<Country>()))
                .ReturnsAsync(country1);
            _countriesRepositoryMock.Setup(c => c.GetCountryByCountryNameAsync(It.IsAny<string>()))
                .ReturnsAsync(null as Country);


            CountryResponse first_country_from_add_country = await _countriesService.AddCountry(request1);
            //Act
            Func<Task> action = async () =>
            {
                _countriesRepositoryMock.Setup(c => c.AddCountryAsync(It.IsAny<Country>()))
                    .ReturnsAsync(country1);
                _countriesRepositoryMock.Setup(c => c.GetCountryByCountryNameAsync(It.IsAny<string>()))
                    .ReturnsAsync(country1);
                await _countriesService.AddCountry(request2);
            };
            //Assert
            await action.Should().ThrowAsync<ArgumentException>();
        }
        //When you supply proper country name, it should inset (add) the country to the existing list of countries
        [Fact]
        public async Task AddCountry_FullCountry_ToBeSuccessful()
        {
            //Arrange
            CountryAddRequest country_request = _fixture.Build<CountryAddRequest>()
                .Create();
            Country country = country_request.ToCountry();
            CountryResponse country_resposne = country.ToCountryResponse();

            _countriesRepositoryMock.Setup(c => c.GetCountryByCountryNameAsync(It.IsAny<string>()))
                .ReturnsAsync(null as Country);
            _countriesRepositoryMock.Setup(c => c.AddCountryAsync(It.IsAny<Country>()))
                .ReturnsAsync(country);

            //Act
            CountryResponse country_from_add_country = await _countriesService.AddCountry(country_request);

            country.CountryId = country_from_add_country.CountryId;
            country_resposne.CountryId = country_from_add_country.CountryId;
            //Assert
            country_from_add_country.CountryId.Should().NotBe(Guid.Empty);
            country_from_add_country.Should().BeEquivalentTo(country_resposne);
            //Assert.Contains比较的时候使用了objA.Equals(objB),是引用类型的比较，不是值的比较
            //所以需要在CountryResponse类中重写Equals
        }
        #endregion

        #region GetAllCountries
        //The list of countries should be empty by default(before adding any countries)
        [Fact]
        public async Task GetAllCountries_ToBeEmptyList()
        {
            //Acts 
            List<Country> country_empty_list = new List<Country>();
            _countriesRepositoryMock.Setup(c => c.GetAllCountriesAsync()).ReturnsAsync(country_empty_list);
            //Act
            List<CountryResponse> actual_country_response_list = await _countriesService.GetAllCountries();
            //Assert
            actual_country_response_list.Should().BeEmpty();    
        }
        //judge if less countries is added
        [Fact]
        public async Task GetAllCountries_ShouldHaveFewCountries()
        {
            //Arrange
            List<Country> country_list = new List<Country>() {
                    _fixture.Build<Country>()
                    .With(temp => temp.Persons, null as List<Person>).Create(),
                    _fixture.Build<Country>()
                    .With(temp => temp.Persons, null as List<Person>).Create()
                  };

            List<CountryResponse> country_response_list = country_list.Select(temp => temp.ToCountryResponse()).ToList();

            _countriesRepositoryMock.Setup(temp => temp.GetAllCountriesAsync()).ReturnsAsync(country_list);

            //Act
            List<CountryResponse> actualCountryResponseList = await _countriesService.GetAllCountries();

            //Assert
            actualCountryResponseList.Should().BeEquivalentTo(country_response_list);
        }
        #endregion

        #region GetCountryByCountryId
        //If we supply null as CountryId, it should return null as CountryResponse
        [Fact]
        public async Task GetCountryByCountryId_NullCountryId_ToBeNull()
        {
            //Arrange
            Guid? countryId = null;

            _countriesRepositoryMock.Setup(c => c.GetCountryByCountryIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(null as Country);
            //Act
            CountryResponse? countryResponse = await _countriesService.GetCountryByCountryId(countryId);

            //Assert
            countryResponse.Should().BeNull();  
        }
        //If we supply a valid country id, it should return the matching country details as CountryResponse object
        [Fact]
        public async Task GetCountryByCountryId_ValidCountryId_ToBeSuccessful()
        {
            //Arrange
            Country country = _fixture.Build<Country>()
                .With(c => c.Persons, null as List<Person>)
                .Create();

            CountryResponse countryResponse = country.ToCountryResponse();

            _countriesRepositoryMock.Setup(c => c.GetCountryByCountryIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(country);

            //Act
            CountryResponse? country_response_from_get = await _countriesService.GetCountryByCountryId(country.CountryId);

            //Assert
            country_response_from_get.Should().Be(countryResponse);
        }
        #endregion
    }
}
